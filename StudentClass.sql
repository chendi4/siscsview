WITH comsect AS (
SELECT a.strm,a.CLASS_SECTION ,a.SUBJECT ,a.CATALOG_NBR ,a.CLASS_NBR ,a.ASSOCIATED_CLASS, 
(CASE WHEN substr(a.INSTRUCTION_MODE,1,1)<>substr(b.INSTRUCTION_MODE,1,1) THEN 'Hybrid'
WHEN substr(a.INSTRUCTION_MODE,1,1)='P' THEN 'In-Person'
WHEN substr(a.INSTRUCTION_MODE,1,1)='O' THEN 'Online'
END ) AS combined_instructor_mode
FROM siscs.C_CLASS_TBL a
LEFT JOIN SISCS.C_CLASS_TBL b
ON a.CRSE_ID =b.CRSE_ID 
AND a.STRM =b.STRM 
AND a.Class_Section=b.CLASS_SECTION 
AND a.ASSOCIATED_CLASS =b.ASSOCIATED_CLASS 
AND b.CLASS_TYPE ='N'
AND b.EDW_ACTV_IND ='Y' AND b.EDW_CURR_IND ='Y'
where a.CLASS_TYPE ='E'
AND a.EDW_ACTV_IND ='Y' AND a.EDW_CURR_IND ='Y'
UNION  --non-enrollable
SELECT a.strm,a.CLASS_SECTION ,a.SUBJECT ,a.CATALOG_NBR ,a.CLASS_NBR ,a.ASSOCIATED_CLASS, 
(CASE 
WHEN substr(a.INSTRUCTION_MODE,1,1)='H' THEN 'hybrid'
WHEN substr(a.INSTRUCTION_MODE,1,1)='P' THEN 'In-Person'
WHEN substr(a.INSTRUCTION_MODE,1,1)='O' THEN 'Online'
END ) AS combined_instructor_mode
FROM siscs.C_CLASS_TBL a
where a.CLASS_TYPE ='N'
AND EDW_ACTV_IND ='Y' AND EDW_CURR_IND ='Y'
)

              select    
              --Key Fields
              r_class_tbl_se_vw_r.emplid
             ,r_class_tbl_se_vw_r.acad_career
             ,r_class_tbl_se_vw_r.institution
             ,r_class_tbl_se_vw_r.strm
             ,r_class_tbl_se_vw_r.class_nbr
             ,nvl(c_crse_attr_value.crse_attr_value,' ') as location_detail
              --Non Key Fields
             ,s_term_val_tbl.descrshort as term_descrshort
             ,r_class_tbl_se_vw_r.session_code
             ,session_code.xlatlongname as session_code_descr
             ,r_class_tbl_se_vw_r.acad_prog as primary_acad_prog
             ,r_class_tbl_se_vw_r.subject
             ,r_class_tbl_se_vw_r.catalog_nbr
             ,trim(r_class_tbl_se_vw_r.catalog_nbr) as crse_code
             ,r_class_tbl_se_vw_r.crse_grade_off
             ,r_class_tbl_se_vw_r.crse_grade_input
             ,r_class_tbl_se_vw_r.dyn_class_nbr
             ,r_class_tbl_se_vw_r.class_section
             ,r_class_tbl_se_vw_r.crse_id
             ,r_class_tbl_se_vw_r.strm || r_class_tbl_se_vw_r.class_nbr as section_id
             ,r_class_tbl_se_vw_r.crse_offer_nbr
             ,r_class_tbl_se_vw_r.crse_career
             ,r_class_tbl_se_vw_r.earn_credit
             ,r_class_tbl_se_vw_r.include_in_gpa
             ,r_class_tbl_se_vw_r.stdnt_enrl_status
             ,stdnt_enrl_status.xlatlongname as stdnt_enrl_status_descr
             ,r_class_tbl_se_vw_r.repeat_code
             ,r_class_tbl_se_vw_r.unt_taken
             ,r_class_tbl_se_vw_r.unt_billing
             ,r_class_tbl_se_vw_r.unt_earned
             ,r_class_tbl_se_vw_r.unt_prgrss
             ,r_class_tbl_se_vw_r.grading_basis_enrl
             ,r_class_tbl_se_vw_r.descr as course_title_descr
             ,r_class_tbl_se_vw_r.location
             ,c_crse_attr_value.descrformal as location_detail_descr
             ,r_class_tbl_se_vw_r.enrl_add_dt
             ,r_class_tbl_se_vw_r.enrl_drop_dt
             ,r_class_tbl_se_vw_r.enrl_status_reason
             ,enrl_status_reason.xlatshortname as enrl_reason_descrshort
             ,enrl_status_reason.xlatlongname as enrl_reason_descrlong
             ,case
                when c_crse_attributes.crse_attr = 'HON' then
                  'Y'
                else
                  'N'
              end as honors_course
             ,case
                when c_class_attribute_hon.crse_attr = 'HON' then
                  'Y'
                else
                  'N'
              end as honors_section
             ,case
                when r_class_tbl_se_vw_r.rqmnt_designtn = 'H' then
                  'Y'
                else
                  'N'
              end as honors_option
	,r_class_tbl_se_vw_r.rqmnt_designtn 
             ,r_class_tbl_se_vw_r.stdnt_positin
             ,r_class_tbl_se_vw_r.instruction_mode
             ,(case when regexp_replace(r_class_tbl_se_vw_r.catalog_nbr, '[^0-9]', '')>=500 
            and  regexp_replace(r_class_tbl_se_vw_r.catalog_nbr, '[^0-9]', '')<=999 
            and (r_class_tbl_se_vw_r.subject <> 'LAW' or r_class_tbl_se_vw_r.unt_taken >= 2) then 'Y' else 'N' end) as honors_exp
             ,r_class_tbl_se_vw_r.class_type
             ,r_class_tbl_se_vw_r.grading_scheme_enr
             ,r_class_tbl_se_vw_r.class_type
             ,b.SSR_COMPONENT 
             ,comsect.combined_instructor_mode
--             ,l_program_nm edw_created_by
--             ,l_program_tm edw_create_date
--             ,l_program_nm edw_last_updated_by
--             ,l_program_tm edw_last_update_date
    from      siscs.r_class_tbl_se_vw_r
    LEFT JOIN (SELECT * FROM siscs.C_CLASS_ASSOC WHERE edw_actv_ind='Y' and edw_curr_ind='Y' )   b
     ON r_class_tbl_se_vw_r.CRSE_ID =b.CRSE_ID 
     AND r_class_tbl_se_vw_r.CRSE_OFFER_NBR =b.CRSE_OFFER_NBR 
     AND r_class_tbl_se_vw_r.STRM =b.strm 
     AND r_class_tbl_se_vw_r.SESSION_CODE =b.SESSION_CODE 
     AND r_class_tbl_se_vw_r.ASSOCIATED_CLASS =b.ASSOCIATED_CLASS 
    INNER JOIN comsect 
    ON r_class_tbl_se_vw_r.strm = comsect.strm
    AND r_class_tbl_se_vw_r.class_section=comsect.class_section
    AND r_class_tbl_se_vw_r.subject = comsect.subject
    AND r_class_tbl_se_vw_r.CATALOG_NBR= comsect.CATALOG_NBR
    AND  r_class_tbl_se_vw_r.CLASS_NBR=comsect.CLASS_NBR

    left join (select * from siscs.c_crse_attributes where edw_actv_ind='Y' and edw_curr_ind='Y')c_crse_attributes on c_crse_attributes.crse_id = r_class_tbl_se_vw_r.crse_id
                                       and c_crse_attributes.crse_attr = 'HON'
    left join (select * from siscs.c_class_attribute where edw_actv_ind='Y' and edw_curr_ind='Y') c_class_attribute_hon on r_class_tbl_se_vw_r.strm = c_class_attribute_hon.strm
                                                              and c_class_attribute_hon.crse_id = r_class_tbl_se_vw_r.crse_id
                                                              and c_class_attribute_hon.crse_offer_nbr = r_class_tbl_se_vw_r.crse_offer_nbr
                                                              and c_class_attribute_hon.session_code = r_class_tbl_se_vw_r.session_code
                                                              and c_class_attribute_hon.class_section = r_class_tbl_se_vw_r.class_section
                                                              and c_class_attribute_hon.crse_attr = 'HON'
    left join (select * from siscs.c_class_attribute where edw_actv_ind='Y' and edw_curr_ind='Y') c_class_attribute_off on r_class_tbl_se_vw_r.strm = c_class_attribute_off.strm
                                                              and c_class_attribute_off.crse_id = r_class_tbl_se_vw_r.crse_id
                                                              and c_class_attribute_off.crse_offer_nbr = r_class_tbl_se_vw_r.crse_offer_nbr
                                                              and c_class_attribute_off.session_code = r_class_tbl_se_vw_r.session_code
                                                              and c_class_attribute_off.class_section = r_class_tbl_se_vw_r.class_section
                                                              and c_class_attribute_off.crse_attr = 'OFF'
    left join (select * from siscs.c_crse_attr_value where edw_actv_ind='Y' and edw_curr_ind='Y')c_crse_attr_value on c_crse_attr_value.crse_attr_value = c_class_attribute_off.crse_attr_value
                                       and c_crse_attr_value.crse_attr = c_class_attribute_off.crse_attr
    left join (select * from siscs.s_term_val_tbl where edw_actv_ind='Y' and edw_curr_ind='Y')s_term_val_tbl on r_class_tbl_se_vw_r.strm = s_term_val_tbl.strm
    left join (select * from siscs.t_xlatitem where edw_actv_ind='Y' and edw_curr_ind='Y') stdnt_enrl_status on stdnt_enrl_status.fieldvalue = r_class_tbl_se_vw_r.stdnt_enrl_status
                                                  and stdnt_enrl_status.fieldname = 'STDNT_ENRL_STATUS'
    left join (select * from siscs.t_xlatitem where edw_actv_ind='Y' and edw_curr_ind='Y') enrl_status_reason on enrl_status_reason.fieldvalue = r_class_tbl_se_vw_r.enrl_status_reason
                                                   and enrl_status_reason.fieldname = 'ENRL_STATUS_REASON'
    left join (select * from siscs.t_xlatitem where edw_actv_ind='Y' and edw_curr_ind='Y') session_code on session_code.fieldvalue = r_class_tbl_se_vw_r.session_code
                                             and session_code.fieldname = 'SESSION_CODE'
                                             
                                             
    where      
    (c_crse_attributes.effdt = (select   max(c_ed.effdt)
                                          from     siscs.c_crse_attributes c_ed
                                          where    c_crse_attributes.crse_id = c_ed.crse_id
                                          --Actitve records only--
                                          and c_ed.edw_actv_ind='Y' and c_ed.edw_curr_ind='Y'
                                          and      c_ed.effdt <= sysdate
                                         )     
    or        c_crse_attributes.effdt is null)
    and       (c_crse_attr_value.effdt = (select   max(f_ed.effdt)
                                           from     siscs.c_crse_attr_value f_ed
                                           where    c_crse_attr_value.crse_attr = f_ed.crse_attr
                                           --Actitve records only--
                                          and f_ed.edw_actv_ind='Y' and f_ed.edw_curr_ind='Y'
                                           and      f_ed.effdt <= sysdate
                                          )
               or 
               c_crse_attr_value.effdt is null 
              )
    and       (stdnt_enrl_status.effdt = (select   max(g_ed.effdt)
                                         from     siscs.t_xlatitem g_ed
                                         where    stdnt_enrl_status.fieldname = g_ed.fieldname
                                         and      stdnt_enrl_status.fieldvalue = g_ed.fieldvalue
                                         --Actitve records only--
                                          and g_ed.edw_actv_ind='Y' and g_ed.edw_curr_ind='Y'
                                         and      g_ed.effdt <= sysdate
                                        )
              or stdnt_enrl_status.effdt is null )
    and       (enrl_status_reason.effdt = (select   max(h_ed.effdt)
                                          from     siscs.t_xlatitem h_ed
                                          where    enrl_status_reason.fieldname = h_ed.fieldname
                                          and      enrl_status_reason.fieldvalue = h_ed.fieldvalue
                                          --Actitve records only--
                                          and h_ed.edw_actv_ind='Y' and h_ed.edw_curr_ind='Y'
                                          and      h_ed.effdt <= sysdate
                                         )
                or enrl_status_reason.effdt is null)
    and       (session_code.effdt = (select   max(i_ed.effdt)
                                    from     siscs.t_xlatitem i_ed
                                    where    session_code.fieldname = i_ed.fieldname
                                    and      session_code.fieldvalue = i_ed.fieldvalue
                                    --Actitve records only--
                                          and i_ed.edw_actv_ind='Y' and i_ed.edw_curr_ind='Y'
                                    and      i_ed.effdt <= sysdate
                                   )
                or session_code.effdt is null) 
                  
      