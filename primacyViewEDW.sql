SELECT
            p_stdnt_car_term.strm,
            s_term_val_tbl.descrshort    AS term_descrshort,
            p_stdnt_car_term.emplid,
            p_stdnt_car_term.institution,
            p_stdnt_car_term.acad_career,
            p_acad_prog.prog_status,
            (
                CASE
                    WHEN  min(CASE WHEN p_acad_prog.prog_status <> 'AC' THEN 999 else b.fa_primacy_nbr end) OVER (PARTITION BY p_stdnt_car_term.emplid,p_stdnt_car_term.strm,p_stdnt_car_term.institution)=999 then ' '
                    WHEN  min(CASE WHEN p_acad_prog.prog_status <> 'AC' THEN 999 else b.fa_primacy_nbr end) OVER (PARTITION BY p_stdnt_car_term.emplid,p_stdnt_car_term.strm,p_stdnt_car_term.institution)= (CASE WHEN p_acad_prog.prog_status <> 'AC' THEN 999 else b.fa_primacy_nbr end)
                 /*and p_acad_prog.prog_status = 'ac'*/ THEN
                        'Y'
                    ELSE
                        'N'
                END
            ) AS primary_car_flag,
            --p_stdnt_car_term.stdnt_car_nbr,
            p_acad_prog.stdnt_car_nbr,
            p_acad_prog.exp_grad_term, -- sisdev-5235
            p_acad_plan.acad_plan,
            (CASE 
            WHEN  min(CASE WHEN p_acad_prog.prog_status <> 'AC' THEN 999 else b.fa_primacy_nbr end) OVER (PARTITION BY p_stdnt_car_term.emplid,p_stdnt_car_term.strm,p_stdnt_car_term.institution)=999 then ' '
            WHEN  min (CASE WHEN p_acad_prog.prog_status <> 'AC' THEN 999 ELSE p_acad_prog.stdnt_car_nbr END) OVER (PARTITION BY p_stdnt_car_term.strm,p_stdnt_car_term.emplid, p_stdnt_car_term.institution, p_stdnt_car_term.acad_career)=p_acad_prog.stdnt_car_nbr
            AND min (p_acad_plan.plan_sequence) OVER (PARTITION BY p_stdnt_car_term.strm,p_stdnt_car_term.emplid, p_stdnt_car_term.institution, p_stdnt_car_term.acad_career,p_acad_prog.stdnt_car_nbr )=p_acad_plan.plan_sequence
            and min(nvl(regexp_replace(SUBSTR(p_acad_plan.acad_plan , INSTR(p_acad_plan.acad_plan , '_') + 1) , '[a-zA-Z]', ''),999)) over (partition by 
     p_stdnt_car_term.strm,p_stdnt_car_term.emplid, p_stdnt_car_term.institution, p_stdnt_car_term.acad_career,p_acad_prog.stdnt_car_nbr,p_acad_plan.plan_sequence)=nvl(regexp_replace(SUBSTR(p_acad_plan.acad_plan , INSTR(p_acad_plan.acad_plan , '_') + 1) , '[a-zA-Z]', ''),999)
            and  min(s_acad_plan_tbl.degree||SUBSTR(p_acad_plan.acad_plan , 0,INSTR(p_acad_plan.acad_plan , '_') - 1)) over (partition by 
     p_stdnt_car_term.strm,p_stdnt_car_term.emplid, p_stdnt_car_term.institution, p_stdnt_car_term.acad_career,p_acad_prog.stdnt_car_nbr,p_acad_plan.plan_sequence,
     nvl(regexp_replace(SUBSTR(p_acad_plan.acad_plan , INSTR(p_acad_plan.acad_plan , '_') + 1) , '[a-zA-Z]', ''),999) )= s_acad_plan_tbl.degree||SUBSTR(p_acad_plan.acad_plan , 0,INSTR(p_acad_plan.acad_plan , '_') - 1)
            THEN 'Y'
            ELSE 'N'
            END ) AS primary_plan_flag,
           
            s_acad_plan_tbl.descr        AS acad_plan_descr,
            s_acad_plan_tbl.descrshort   AS acad_plan_descrshort,
            p_acad_prog.acad_prog        AS acad_plan_acad_prog,
            s_acad_prog_tbl.descr        AS acad_prog_descr,
            s_acad_prog_tbl.descrshort   AS acad_prog_descrshort,
            s_acad_plan_tbl.degree,
            s_acad_plan_tbl.descr        AS degree_descr,
            s_acad_plan_tbl.descrshort   AS degree_descrshort,
            s_degree_tbl.education_lvl,
            t_xlatitem.xlatlongname education_lvl_xlatlongname ,
            t_xlatitem.xlatshortname education_lvl_xlatshortname 
           -- b.fa_primacy_nbr,
           -- min( b.fa_primacy_nbr) OVER (PARTITION BY p_stdnt_car_term.emplid,p_stdnt_car_term.strm,p_stdnt_car_term.institution) AS minfa
--            l_program_nm                 edw_created_by,
--            l_program_tm                 edw_create_date,
--            l_program_nm                 edw_last_updated_by,
--            l_program_tm                 edw_last_update_date
        FROM
            siscs.p_stdnt_car_term p_stdnt_car_term
     inner JOIN  (  SELECT ACAD_CAREER , FA_PRIMACY_NBR , ROW_NUMBER() OVER (PARTITION BY INSTITUTION, ACAD_CAREER ORDER BY EFFDT) AS cnt 
            FROM siscs.S_ACAD_CAR_TBL sactv WHERE EFFDT <=SYSDATE ) b
	ON p_stdnt_car_term.ACAD_CAREER =b.ACAD_CAREER AND cnt =1
 		 /*LEFT OUTER JOIN (
                SELECT
                    *
                FROM
                    siscs.p_stdnt_equtn_var
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) p_stdnt_equtn_var ON p_stdnt_car_term.emplid = p_stdnt_equtn_var.emplid
                                   AND p_stdnt_car_term.institution = p_stdnt_equtn_var.institution
                                   AND p_stdnt_car_term.strm = p_stdnt_equtn_var.strm
                                   AND p_stdnt_equtn_var.billing_career = p_stdnt_car_term.acad_career*/
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.p_acad_prog
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) p_acad_prog ON p_stdnt_car_term.emplid = p_acad_prog.emplid
                             AND p_stdnt_car_term.acad_career = p_acad_prog.acad_career
                             AND p_stdnt_car_term.institution = p_acad_prog.institution
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.p_acad_plan
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) p_acad_plan ON p_acad_prog.emplid = p_acad_plan.emplid
                             AND p_acad_prog.acad_career = p_acad_plan.acad_career
                             AND p_acad_prog.stdnt_car_nbr = p_acad_plan.stdnt_car_nbr
                             AND p_acad_plan.effdt = p_acad_prog.effdt
                             AND p_acad_prog.effseq = p_acad_plan.effseq
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.s_term_tbl
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) s_term_tbl ON p_stdnt_car_term.acad_career = s_term_tbl.acad_career
                            AND p_stdnt_car_term.institution = s_term_tbl.institution
                            AND p_stdnt_car_term.strm = s_term_tbl.strm
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.s_acad_plan_tbl
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) s_acad_plan_tbl ON p_stdnt_car_term.institution = s_acad_plan_tbl.institution
                                 AND p_acad_plan.acad_plan = s_acad_plan_tbl.acad_plan
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.s_acad_prog_tbl
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) s_acad_prog_tbl ON p_acad_prog.institution = s_acad_prog_tbl.institution
                                 AND p_acad_prog.acad_prog = s_acad_prog_tbl.acad_prog
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.s_term_val_tbl
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) s_term_val_tbl ON p_stdnt_car_term.strm = s_term_val_tbl.strm
            --INNER JOIN siscs.gtt_s_dreg_tbl      gtt_s_dreg_tbl ON s_acad_plan_tbl.degree = gtt_s_dreg_tbl.degree 
            INNER JOIN (
                SELECT
                    *
                FROM
                    siscs.s_degree_tbl
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) s_degree_tbl ON s_acad_plan_tbl.degree = s_degree_tbl.degree
            LEFT OUTER JOIN (
                SELECT
                    *
                FROM
                    siscs.t_xlatitem
                WHERE
                    edw_actv_ind = 'Y' and edw_curr_ind='Y'
            ) t_xlatitem ON s_degree_tbl.education_lvl = t_xlatitem.fieldvalue
                            AND t_xlatitem.fieldname = 'EDUCATION_LVL'
        WHERE
          p_stdnt_car_term.edw_actv_ind = 'Y' and p_stdnt_car_term.edw_curr_ind='Y'
            AND ( s_acad_prog_tbl.effdt = (
                SELECT
                    MAX(g_ed.effdt)
                FROM
                    siscs.s_acad_prog_tbl g_ed
                WHERE
                    g_ed.edw_actv_ind = 'Y'
                    AND s_acad_prog_tbl.institution = g_ed.institution
                    AND s_acad_prog_tbl.acad_prog = g_ed.acad_prog
                    AND g_ed.edw_curr_ind = 'Y'
                    AND g_ed.effdt <= s_term_tbl.term_end_dt --<= s_term_tbl.term_end_dt
            )
                  OR s_acad_prog_tbl.effdt IS NULL )
            AND ( s_acad_plan_tbl.effdt = (
                SELECT
                    MAX(f_ed.effdt)
                FROM
                    siscs.s_acad_plan_tbl f_ed
                WHERE
                    f_ed.edw_actv_ind = 'Y'
                    AND s_acad_plan_tbl.institution = f_ed.institution
                    AND s_acad_plan_tbl.acad_plan = f_ed.acad_plan
                    AND f_ed.edw_curr_ind = 'Y'
                    AND f_ed.effdt <= s_term_tbl.term_end_dt
            )
                  OR s_acad_plan_tbl.effdt IS NULL )
            AND ( p_acad_prog.effdt = (
                SELECT
                    MAX(b_ed.effdt)
                FROM
                    siscs.p_acad_prog b_ed
                WHERE
                    b_ed.edw_actv_ind = 'Y'
                    AND p_acad_prog.emplid = b_ed.emplid
                    AND p_acad_prog.acad_career = b_ed.acad_career
                    AND p_acad_prog.stdnt_car_nbr = b_ed.stdnt_car_nbr
                    AND b_ed.edw_curr_ind = 'Y'
                    AND b_ed.effdt <= s_term_tbl.term_end_dt
            )
                  OR p_acad_prog.effdt IS NULL )
            AND ( p_acad_prog.effseq = (
                SELECT
                    MAX(b_es.effseq)
                FROM
                    siscs.p_acad_prog b_es
                WHERE
                    b_es.edw_actv_ind = 'Y'
                    AND p_acad_prog.emplid = b_es.emplid
                    AND p_acad_prog.acad_career = b_es.acad_career
                    AND p_acad_prog.stdnt_car_nbr = b_es.stdnt_car_nbr
                    AND b_es.edw_curr_ind = 'Y'
                    AND p_acad_prog.effdt = b_es.effdt
            )
                  OR p_acad_prog.effseq IS NULL )                      --ADDED
            AND ( s_degree_tbl.effdt = (
                SELECT
                    MAX(i_ed.effdt)               --ADDED
                FROM
                    siscs.s_degree_tbl i_ed             --ADDED   
                WHERE
                    i_ed.edw_actv_ind = 'Y'
                    AND i_ed.degree = s_degree_tbl.degree         --ADDED
                    AND i_ed.edw_curr_ind = 'Y'
                    AND i_ed.effdt <= s_term_tbl.term_end_dt
            )
                  OR s_degree_tbl.effdt IS NULL )
            AND ( t_xlatitem.effdt = (
                SELECT
                    MAX(x_ed.effdt)             --ADDED
                FROM
                    siscs.t_xlatitem x_ed                    --ADDED
                WHERE
                    x_ed.edw_actv_ind = 'Y'
                    AND x_ed.fieldname = t_xlatitem.fieldname      --ADDED
                    AND x_ed.fieldvalue = t_xlatitem.fieldvalue     --ADDED
                    AND x_ed.edw_curr_ind = 'Y'
                    AND x_ed.effdt <= s_term_tbl.term_end_dt
            )       --ADDED
                  OR t_xlatitem.effdt IS NULL );
            