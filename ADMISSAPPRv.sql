CREATE TABLE GTT_PROG_STATUS_DATA AS (
SELECT
	EMPLID,
	ADM_APPL_NBR,
	(CASE
		WHEN PROG_STATUS IN ('CN', 'AD') THEN 'ZZ'
		ELSE PROG_STATUS
	END) AS EVAL_STATUS,
	PROG_STATUS ,
	PROG_ACTION,
	PROG_REASON,
	EFFDT,
	EFFSEQ,
	ROW_NUMBER() OVER (PARTITION BY EMPLID,
	ADM_APPL_NBR,
	(CASE
		WHEN PROG_STATUS IN ('CN', 'AD') THEN 'ZZ'
		ELSE PROG_STATUS
	END)
ORDER BY
	EFFDT DESC,
	EFFSEQ DESC) AS EVAL_STATUS_RANK
FROM
	SISCS.P_ADM_APPL_PROG_AV
WHERE
	edw_actv_ind = 'Y'
	AND edw_curr_ind = 'Y'
	AND prog_status IN ( 'AP', 'CN', 'AD', 'PM' )
)


CREATE TABLE GTT_GRADE_DATA AS 
(select  emplid,
                ext_summ_type,
                MAX(ext_gpa) AS hs_gpa,
                MAX(percentile) AS percentile,
                MAX(class_rank) AS class_rank,
                MAX(class_size) AS class_size
            FROM
                siscs.p_ext_acad_sum_av
            WHERE
                edw_actv_ind = 'Y'
             AND edw_curr_ind = 'Y'
             and ext_career = 'HS'
             AND ext_summ_type IN (
                    'HGPA',
                    'HGPU',
                    'HGPW',
                    'MSCG'
                )
            GROUP BY
                emplid,
                ext_summ_type
)


CREATE TABLE MSU_RACE AS (
SELECT		
p_citizenship.emplid,		
'Hispanic/Latino' AS ipeds_group,		
'H' as ipeds_code,		
'Hispanic' as ipeds_group_short		
FROM		
siscs.p_citizenship_av p_citizenship		
WHERE		
p_citizenship.country = 'USA'		
AND edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
AND EXISTS (		
SELECT		
p_ethnicity_dtl.emplid		
FROM		
siscs.p_ethnicity_dtl_av p_ethnicity_dtl		
WHERE		
p_ethnicity_dtl.emplid = p_citizenship.emplid		
AND p_ethnicity_dtl.hisp_latino = 'Y'		
AND p_ethnicity_dtl.edw_actv_ind = 'Y'		
AND p_ethnicity_dtl.edw_curr_ind = 'Y'		
)		
UNION		
SELECT		
p_citizenship.emplid,		
CASE		
WHEN length(b.ethnic_list) > 1 THEN		
'Two or More Races' 		
ELSE		
t_xlattable_vw.xlatlongname		
END AS ipeds_group	,	
case		
when length(b.ethnic_list) > 1 then	'M'	
when	t_xlattable_vw.XLATSHORTNAME	='Am. Indian' then '5'
when	t_xlattable_vw.XLATSHORTNAME	='Asian' then '11'
when	t_xlattable_vw.XLATSHORTNAME	='Black' then '2'
when	t_xlattable_vw.XLATSHORTNAME	='Hawaiian' then '10'
when	t_xlattable_vw.XLATSHORTNAME	='NS' then 'N'
when	t_xlattable_vw.XLATSHORTNAME	='White' then '1'
end as ipeds_code,		
case		
when length(b.ethnic_list) > 1 then	'Multiple'	
when	t_xlattable_vw.XLATSHORTNAME	='Am. Indian' then 'Amer Ind'
when	t_xlattable_vw.XLATSHORTNAME	='Hawaiian' then 'Hawaiian/PI'
when	t_xlattable_vw.XLATSHORTNAME	='NS' then 'Not Spcfd'
else	t_xlattable_vw.XLATSHORTNAME	
end as ipeds_group_short		
FROM		
(		
SELECT		
*		
FROM		
siscs.p_citizenship_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) p_citizenship, (		
SELECT		
z.emplid,		
LISTAGG(z.ethnic_group, '') WITHIN GROUP(		
ORDER BY		
z.ethnic_group		
) AS ethnic_list		
FROM		
(		
SELECT DISTINCT		
p_ethnicity_dtl.emplid,		
s_ethnic_grp_tbl.ethnic_group		
FROM		
(		
SELECT		
*		
FROM		
siscs.p_ethnicity_dtl_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) p_ethnicity_dtl,		
(		
SELECT		
*		
FROM		
siscs.s_ethnic_grp_tbl_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) s_ethnic_grp_tbl		
WHERE		
p_ethnicity_dtl.ethnic_grp_cd = s_ethnic_grp_tbl.ethnic_grp_cd		
AND s_ethnic_grp_tbl.setid = 'USA'		
AND s_ethnic_grp_tbl.effdt = (		
SELECT		
MAX(b_ed.effdt)		
FROM		
(		
SELECT		
*		
FROM		
siscs.s_ethnic_grp_tbl_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) b_ed		
WHERE		
s_ethnic_grp_tbl.setid = b_ed.setid		
AND s_ethnic_grp_tbl.ethnic_grp_cd = b_ed.ethnic_grp_cd		
AND b_ed.effdt <= sysdate		
)		
AND NOT EXISTS (		
SELECT		
p_ethnicity_dtl_hispy.emplid		
FROM		
(		
SELECT		
*		
FROM		
siscs.p_ethnicity_dtl_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) p_ethnicity_dtl_hispy		
WHERE		
p_ethnicity_dtl_hispy.emplid = p_ethnicity_dtl.emplid		
AND p_ethnicity_dtl_hispy.hisp_latino = 'Y'		
)		
) z		
GROUP BY		
z.emplid		
) b		
LEFT OUTER JOIN (		
SELECT		
*		
FROM		
siscs.t_xlattable_vw_av		
WHERE		
edw_actv_ind = 'Y'		
AND edw_curr_ind = 'Y'		
) t_xlattable_vw ON t_xlattable_vw.fieldname = 'ETHNIC_GROUP'		
AND t_xlattable_vw.fieldvalue = b.ethnic_list		
AND t_xlattable_vw.eff_status = 'A'		
WHERE		
p_citizenship.emplid = b.emplid		
AND p_citizenship.country = 'USA'
)

--DROP TABLE stdnt_data

CREATE TABLE stdnt_data AS 
(select p_names.emplid
,p_names.name as pri_name
,p_names_prf.name as prf_name
,p_person.birthdate
,p_citizenship.citizenship_status
,p_citizenship.country
,s_citizen_sts_tbl.descrshort as ctzn_sts_descr
,p_pers_data_effdt.sex 
from siscs.p_names  p_names 
left outer join (select * from siscs.p_names_av where edw_actv_ind = 'Y' and edw_curr_ind='Y') p_names_prf
on p_names.emplid = p_names_prf.emplid
and p_names_prf.name_type = 'PRF' 
inner join (select * from siscs.p_person_av where edw_actv_ind = 'Y' and edw_curr_ind='Y') p_person
on p_names.emplid = p_person.emplid
inner join (select * from siscs.p_citizenship_av where edw_actv_ind = 'Y' and edw_curr_ind='Y') p_citizenship
on p_names.emplid = p_citizenship.emplid
inner join(select * from siscs.s_citizen_sts_tbl where edw_actv_ind = 'Y' and edw_curr_ind='Y') s_citizen_sts_tbl
on p_citizenship.citizenship_status = s_citizen_sts_tbl.citizenship_status
and p_citizenship.country = s_citizen_sts_tbl.country
inner join (select * from siscs.p_pers_data_effdt_av where edw_actv_ind = 'Y' and edw_curr_ind='Y') p_pers_data_effdt
on p_names.emplid = p_pers_data_effdt.emplid
where p_names.edw_actv_ind = 'Y' and p_names.edw_curr_ind='Y'
and p_names.name_type = 'PRI'
and p_names.effdt = ( select max(a_ed.effdt)
from siscs.p_names_av a_ed
where p_names.emplid = a_ed.emplid
and p_names.name_type = a_ed.name_type
and a_ed.edw_actv_ind = 'Y'
and a_ed.edw_curr_ind = 'Y'
and a_ed.effdt <= sysdate)
and ( p_names_prf.effdt = ( select max(b_ed.effdt)
from siscs.p_names b_ed
where p_names_prf.emplid = b_ed.emplid
and p_names_prf.name_type = b_ed.name_type
and b_ed.edw_actv_ind = 'Y'
and b_ed.edw_curr_ind = 'Y'
and b_ed.effdt <= sysdate)
or p_names_prf.effdt is null )
and p_pers_data_effdt.effdt = ( select max(f_ed.effdt)
from siscs.p_pers_data_effdt f_ed
where p_pers_data_effdt.emplid = f_ed.emplid
and f_ed.edw_actv_ind = 'Y'
and f_ed.edw_curr_ind = 'Y'
and f_ed.effdt <= sysdate)

)

CREATE TABLE admit_data AS (
select p_adm_appl_prog.emplid
,p_adm_appl_prog.acad_career
,s_term_tbl.descrshort as appl_in_term
,p_adm_appl_prog.admit_term as appl_in_strm
from (select emplid, acad_career,institution, min(admit_term) AS admit_term from siscs.p_adm_appl_prog_av 
where edw_actv_ind = 'Y' and edw_curr_ind='Y'
AND prog_action = 'APPL'
and prog_reason = 'NEW'
GROUP BY emplid, acad_career, institution
)p_adm_appl_prog
inner join (select * from siscs.s_term_tbl_av where edw_actv_ind = 'Y' and edw_curr_ind='Y')s_term_tbl
on p_adm_appl_prog.institution = s_term_tbl.institution
and p_adm_appl_prog.admit_term = s_term_tbl.strm
AND p_adm_appl_prog.acad_career = s_term_tbl.acad_career
)

CREATE TABLE ext_acad_data AS (
select emplid
,ext_org_id
,ext_career
,ext_data_nbr
,from_dt
,to_dt
,row_number() over(partition by emplid,ext_org_id order by to_dt desc ) as ext_career_rank
from siscs.p_ext_acad_data_av  p_ext_acad_data
where p_ext_acad_data.edw_actv_ind = 'Y'
and p_ext_acad_data.edw_curr_ind = 'Y')


CREATE TABLE org_data AS (
select s_acad_plan_tbl.institution
,s_acad_plan_tbl.acad_plan
,s_acad_plan_owner.acad_org
,s_acad_plan_tbl.diploma_descr
,c.parent_acad_org
,c.parent_descrformal
,c.child_acad_org
,c.child_descr_formal
from   (select * from siscs.s_acad_plan_tbl_av where edw_actv_ind = 'Y' and edw_curr_ind='Y') s_acad_plan_tbl
left outer join (select * from siscs.s_acad_plan_owner_av where edw_actv_ind = 'Y' and edw_curr_ind='Y')s_acad_plan_owner
on s_acad_plan_tbl.institution = s_acad_plan_owner.institution
and s_acad_plan_tbl.acad_plan = s_acad_plan_owner.acad_plan 
left outer join siscs.r_acadorg_rv  c
on c.child_acad_org = s_acad_plan_owner.acad_org 
where  (s_acad_plan_tbl.effdt = ( select max(a_ed.effdt)
from siscs.s_acad_plan_tbl a_ed
where s_acad_plan_tbl.institution = a_ed.institution
and s_acad_plan_tbl.acad_plan = a_ed.acad_plan                                    
and a_ed.edw_actv_ind = 'Y'
and a_ed.edw_curr_ind = 'Y'    
and a_ed.effdt <= sysdate)
or s_acad_plan_tbl.effdt is null)
and ( s_acad_plan_owner.effdt = ( select max(b_ed.effdt)
from siscs.s_acad_plan_owner b_ed
where s_acad_plan_owner.institution = b_ed.institution
and s_acad_plan_owner.acad_plan = b_ed.acad_plan                                    
and b_ed.edw_actv_ind = 'Y'
and b_ed.edw_curr_ind = 'Y' 
and b_ed.effdt <= s_acad_plan_tbl.effdt
)
or s_acad_plan_owner.effdt is null ) 
)

CREATE TABLE TestSS AS (
SELECT *
FROM (
SELECT emplid, test_id, TEST_COMPONENT , score
FROM siscs.P_STDNT_TEST_COMP_V 
WHERE TEST_ID='HIGHSAT'
)
pivot
(avg(score)
FOR TEST_COMPONENT 
IN ('TOTAL' AS HIGHSAT_TOTAL,'SSMAT' AS HIGHSAT_SSMAT ,'SSRDW' AS HIGHSAT_SSRDW ,'SSTOT' AS HIGHSAT_SSTOT)
)
)

SELECT  stdnt_data.emplid
,p_adm_appl_data.acad_career
,p_adm_appl_data.stdnt_car_nbr
,p_adm_appl_prog.admit_term
,s_term_tbl.descrshort admit_term_descrshort
,p_adm_appl_data.admit_type
,case
when exists ( select 'x'
from siscs.p_adm_appl_prog_av i
where i.emplid = p_adm_appl_data.emplid
and i.acad_career = p_adm_appl_data.acad_career
and i.stdnt_car_nbr = p_adm_appl_data.stdnt_car_nbr
and i.adm_appl_nbr = p_adm_appl_data.adm_appl_nbr
and i.institution = p_adm_appl_data.institution
and i.prog_status = 'AD'
) then 'Y'
else 'N'
end as admitted_flag
,p_adm_appl_prog.acad_prog
,p_adm_appl_plan.acad_plan
,org_data.diploma_descr as acad_plan_diploma_descr
,p_adm_appl_data.adm_appl_nbr
,p_adm_appl_data.adm_appl_ctr
,p_adm_appl_data.adm_appl_method
,p_adm_appl_data.app_fee_status
,stdnt_data.pri_name
,(CASE WHEN stdnt_data.prf_name IS NOT NULL THEN  stdnt_data.prf_name ELSE stdnt_data.pri_name END ) AS prf_name
,stdnt_data.citizenship_status
,stdnt_data.ctzn_sts_descr
,stdnt_data.sex
,p_residency_off.admission_res
,p_residency_off.city AS RES_CITY 
,p_residency_off.country AS RES_COUNTRY
,p_residency_off.county AS RES_COUNTY
,p_residency_off.state AS RES_STATE
,p_residency_off.FIN_AID_FED_RES
,p_residency_off.tuition_res
,s_residency_tbl.descrshort as adm_res_descrshort
,(CASE WHEN stdnt_data.citizenship_status='4' AND stdnt_data.country = 'USA' THEN 'International' ELSE msu_race.IPEDS_GROUP end) AS IPEDS_RACE_ETHNICITY
,(CASE WHEN stdnt_data.citizenship_status='4' AND stdnt_data.country = 'USA' THEN 'T' ELSE msu_race.IPEDS_Code end) AS IPEDS_Code
,(CASE WHEN stdnt_data.citizenship_status='4' AND stdnt_data.country = 'USA' THEN 'Intl' ELSE msu_race.ipeds_group_short end) AS IPEDS_Short_descr
,msu_race.IPEDS_GROUP AS race_ethnicity
,msu_race.IPEDS_Code AS RE_Code
,msu_race.ipeds_group_short AS RE_SHORT_DESCR
--,r_stdntbiodemo_r.race_ethnicity -- SISDEV-2715
,stdnt_data.birthdate
,s_ext_org_tbl_adm.ls_school_type
,p_adm_appl_data.last_sch_attend
,ext_acad_data.from_dt
,ext_acad_data.to_dt
,prog_status_data_ap.prog_status as app_prog_status
,prog_status_data_ap.prog_action as app_prog_action
,prog_status_data_ap.prog_reason as app_prog_reason
,prog_status_data_ap.effdt as app_effdt
,prog_status_data_cn.prog_status as dcn_prog_status
,nvl(prog_status_data_cn.prog_action,' ') as dcn_prog_action
,prog_status_data_cn.prog_reason as dcn_prog_reason
,prog_status_data_cn.effdt as dcn_effdt
,prog_status_data_pm.prog_status as acpt_prog_status
,prog_status_data_pm.prog_action as acpt_prog_action
,prog_status_data_pm.prog_reason as acpt_prog_reason
,prog_status_data_pm.effdt as acpt_effdt
,p_adm_appl_prog.prog_status as curr_prog_status
,p_adm_appl_prog.prog_action as curr_prog_action
,p_adm_appl_prog.prog_reason as curr_prog_reason
,p_adm_appl_prog.effdt as curr_effdt
,org_data.parent_acad_org as plan_parent_acad_org
,org_data.parent_descrformal as plan_parent_descrformal
,org_data.child_acad_org as plan_child_acad_org
,org_data.child_descr_formal as plan_child_descr_formal
,grade_data_hgpa.hs_gpa as hs_gpa
,grade_data_hgpu.hs_gpa as hs_gpa_unwtd
,grade_data_hgpw.hs_gpa as hs_gpa_wghtd
,grade_data_mscg.hs_gpa as msucgpa
,grade_data_mscg.percentile
,grade_data_mscg.class_rank
,grade_data_mscg.class_size
,case
when exists ( select 'x'
from siscs.p_adm_appl_rcr_ca h
where h.emplid = p_adm_appl_data.emplid
and h.acad_career = p_adm_appl_data.acad_career
and h.stdnt_car_nbr = p_adm_appl_data.stdnt_car_nbr
and h.adm_appl_nbr = p_adm_appl_data.adm_appl_nbr
and h.institution = p_adm_appl_data.institution
and h.recruitment_cat = 'FGEN'
) then 'Y'
else 'N'
end as rcr_fgen
,admit_data.appl_in_term
,admit_data.appl_in_strm
,TESTSS.HIGHSAT_TOTAL
,TESTSS.HIGHSAT_SSMAT 
,TESTSS.HIGHSAT_SSRDW 
, TESTSS.HIGHSAT_SSTOT
from (select * from siscs.p_adm_appl_data_av where edw_actv_ind='Y' and edw_curr_ind='Y') p_adm_appl_data
inner join (select * from siscs.p_adm_appl_prog_av where edw_actv_ind='Y' and edw_curr_ind='Y') p_adm_appl_prog
on  p_adm_appl_data.emplid = p_adm_appl_prog.emplid
and p_adm_appl_data.acad_career = p_adm_appl_prog.acad_career
and p_adm_appl_data.stdnt_car_nbr = p_adm_appl_prog.stdnt_car_nbr
and p_adm_appl_data.adm_appl_nbr = p_adm_appl_prog.adm_appl_nbr
inner join (select * from siscs.p_adm_appl_plan_av where edw_actv_ind='Y' and edw_curr_ind='Y')p_adm_appl_plan
on  p_adm_appl_prog.emplid = p_adm_appl_plan.emplid
and p_adm_appl_prog.acad_career = p_adm_appl_plan.acad_career
and p_adm_appl_prog.stdnt_car_nbr = p_adm_appl_plan.stdnt_car_nbr
and p_adm_appl_prog.adm_appl_nbr = p_adm_appl_plan.adm_appl_nbr
and p_adm_appl_prog.appl_prog_nbr = p_adm_appl_plan.appl_prog_nbr
and p_adm_appl_prog.effdt = p_adm_appl_plan.effdt
and p_adm_appl_prog.effseq = p_adm_appl_plan.effseq
inner join (select * from siscs.s_term_tbl_av  where edw_actv_ind='Y' and edw_curr_ind='Y')s_term_tbl
on  p_adm_appl_data.acad_career = s_term_tbl.acad_career
and p_adm_appl_data.institution = s_term_tbl.institution
and p_adm_appl_prog.admit_term = s_term_tbl.strm
left join (select * from siscs.p_residency_off_av  where edw_actv_ind='Y' and edw_curr_ind='Y') p_residency_off
on  p_adm_appl_prog.emplid = p_residency_off.emplid
and p_adm_appl_prog.acad_career = p_residency_off.acad_career
and p_adm_appl_prog.institution = p_residency_off.institution
and p_adm_appl_prog.admit_term =  p_residency_off.effective_term
left join (SELECT  residency,  ROW_NUMBER() OVER (PARTITION BY RESIDENCY ORDER BY effdt desc) AS cnter, descr, descrshort from siscs.s_residency_tbl_av  where edw_actv_ind='Y' and edw_curr_ind='Y' AND effdt <= SYSDATE) s_residency_tbl
on  s_residency_tbl.residency = p_residency_off.admission_res
and  s_residency_tbl.cnter=1
left join (SELECT ext_org_id, ls_school_type, ROW_NUMBER() OVER (PARTITION BY ext_org_id ORDER BY effdt DESC) AS cnter   from siscs.s_ext_org_tbl_adm_av where edw_actv_ind='Y' and edw_curr_ind='Y' AND effdt <= sysdate) s_ext_org_tbl_adm
on  s_ext_org_tbl_adm.ext_org_id = p_adm_appl_data.last_sch_attend
and s_ext_org_tbl_adm.cnter=1
LEFT JOIN stdnt_data 
on p_adm_appl_data.emplid = stdnt_data.emplid
LEFT JOIN org_data 
on p_adm_appl_prog.institution = org_data.institution
and p_adm_appl_plan.acad_plan = org_data.acad_plan
LEFT JOIN MSU_Race 
ON p_adm_appl_data.emplid = MSU_RACE.emplid
left outer join gtt_prog_status_data prog_status_data_ap
on   p_adm_appl_data.emplid = prog_status_data_ap.emplid
and p_adm_appl_data.adm_appl_nbr = prog_status_data_ap.adm_appl_nbr
and prog_status_data_ap.eval_status = 'AP'
and prog_status_data_ap.eval_status_rank = 1 
left outer join gtt_prog_status_data prog_status_data_cn
on p_adm_appl_data.emplid = prog_status_data_cn.emplid
and p_adm_appl_data.adm_appl_nbr = prog_status_data_cn.adm_appl_nbr
and prog_status_data_cn.eval_status = 'ZZ'
and prog_status_data_cn.eval_status_rank = 1 
left outer join gtt_prog_status_data prog_status_data_pm
on  p_adm_appl_data.emplid = prog_status_data_pm.emplid
and p_adm_appl_data.adm_appl_nbr = prog_status_data_pm.adm_appl_nbr
and prog_status_data_pm.eval_status = 'PM'
and prog_status_data_pm.eval_status_rank = 1
left outer JOIN gtt_grade_data grade_data_hgpa
on  p_adm_appl_data.emplid = grade_data_hgpa.emplid
and grade_data_hgpa.ext_summ_type = 'HGPA' 
left outer join gtt_grade_data grade_data_hgpu
on  p_adm_appl_data.emplid = grade_data_hgpu.emplid
and grade_data_hgpu.ext_summ_type = 'HGPU' 
left outer join gtt_grade_data grade_data_hgpw
on  p_adm_appl_data.emplid = grade_data_hgpw.emplid
and grade_data_hgpw.ext_summ_type = 'HGPW' 
left outer join gtt_grade_data grade_data_mscg
on  p_adm_appl_data.emplid = grade_data_mscg.emplid
and grade_data_hgpw.ext_summ_type = 'MSCG'
left outer join ( select emplid
,ext_org_id
,ext_career
,ext_data_nbr
,from_dt
,to_dt
,row_number() over(partition by emplid,ext_org_id order by to_dt desc ) as ext_career_rank
from siscs.p_ext_acad_data_av p_ext_acad_data
where p_ext_acad_data.edw_actv_ind = 'Y'
and p_ext_acad_data.edw_curr_ind = 'Y') ext_acad_data
on  p_adm_appl_data.emplid = ext_acad_data.emplid
and p_adm_appl_data.last_sch_attend = ext_acad_data.ext_org_id
and ext_career_rank = 1 
LEFT JOIN admit_data
on  p_adm_appl_data.emplid = admit_data.emplid
and p_adm_appl_data.acad_career = admit_data.acad_career 
LEFT JOIN TestSS 
ON TestSS.emplid= p_adm_appl_data.emplid

WHERE (p_adm_appl_prog.effdt = ( select max(b_ed.effdt)
from siscs.p_adm_appl_prog b_ed
where p_adm_appl_prog.emplid = b_ed.emplid
and p_adm_appl_prog.acad_career = b_ed.acad_career
and p_adm_appl_prog.stdnt_car_nbr = b_ed.stdnt_car_nbr
and p_adm_appl_prog.adm_appl_nbr = b_ed.adm_appl_nbr
and p_adm_appl_prog.appl_prog_nbr = b_ed.appl_prog_nbr
and b_ed.edw_actv_ind = 'Y'
and b_ed.edw_curr_ind = 'Y'
and b_ed.effdt <= sysdate)
or p_adm_appl_prog.effdt is null)
and (p_adm_appl_prog.effseq = ( select max(b_es.effseq)
from siscs.p_adm_appl_prog b_es
where p_adm_appl_prog.emplid = b_es.emplid
and p_adm_appl_prog.acad_career = b_es.acad_career
and p_adm_appl_prog.stdnt_car_nbr = b_es.stdnt_car_nbr
and p_adm_appl_prog.adm_appl_nbr = b_es.adm_appl_nbr
and p_adm_appl_prog.appl_prog_nbr = b_es.appl_prog_nbr
and b_es.edw_actv_ind = 'Y'
and b_es.edw_curr_ind = 'Y'
and p_adm_appl_prog.effdt = b_es.effdt)
or p_adm_appl_prog.effseq is null)